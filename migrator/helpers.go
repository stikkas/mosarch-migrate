package main

import (
	"database/sql"
	"log"
	"strings"
)

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func rollback(err error, tx *sql.Tx) {
	if err != nil {
		tx.Rollback()
		log.Fatal(err)
	}
}

func getIdByName(dict []*Dict, name string) int {
	for i := range dict {
		if strings.ToLower(dict[i].name) == name {
			return dict[i].id
		}
	}
	return 0
}
