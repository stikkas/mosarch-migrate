package main

import (
	"database/sql"
	"fmt"
	"github.com/akamensky/argparse"
	_ "github.com/denisenkom/go-mssqldb"
	_ "github.com/lib/pq"
	"math/rand"
	"os"
	"pkg.re/essentialkaos/translit.v2"
	"strings"
)

const (
	sqlserver       = "sqlserver"
	postgres        = "postgres"
	defaultPassword = "$2a$10$WUZP4VTyoAARxMSNHkprwuoHMF2MSfloroLwsBhLbZskmsNXCH52y" // 123
)

var (
	db         *sql.DB
	fallback   *os.File
	countries  []*Dict
	educations []*Dict
	ranks      []*Dict
	topics     []*Dict
)

func opers(users []*User) string {
	s := make([]string, 0)
	for i := range users {
		s = append(s, fmt.Sprintf("$%v", users[i].clientId))
	}
	return strings.Join(s, ",")
}

func main() {
	parser := argparse.NewParser("rr-migrate", "migration data from old db to new one")
	source := NewPropertyConnection(
		[4]string{"i", "source-host", "localhost", "source database host"},
		[4]string{"p", "source-port", "1433", "source database port"},
		[4]string{"d", "source-db", "rh", "source database"},
		[4]string{"u", "source-user", "SA", "source database username"},
		[4]string{"s", "source-password", "123456qB", "source database password"},
		parser)

	destination := NewPropertyConnection(
		[4]string{"I", "dest-host", "localhost", "destination database host"},
		[4]string{"P", "dest-port", "5432", "destination database port"},
		[4]string{"D", "dest-db", "ais-rr", "destination database"},
		[4]string{"U", "dest-user", "aisrr", "destination database username"},
		[4]string{"S", "dest-password", "AisReadRoo15", "destination database password"},
		parser)

	fallbackFileName := parser.String("f", "file", &argparse.Options{Default: "problem_clients.txt", Help: "File with data of Clients that should be migrated manual."})

	err := parser.Parse(os.Args)
	if err != nil {
		fmt.Print(parser.Usage(err))
	}

	db, err = sql.Open(sqlserver, source.uri(sqlserver))
	check(err)
	db.SetMaxOpenConns(100)
	db.SetMaxIdleConns(10)

	fallback, err = os.Create(*fallbackFileName)
	defer fallback.Close()

	users := selectUsers()
	db.Close()

	count := 0
	clearUsers := make([]*User, 0)
	for _, us := range users {
		count += 1
		exists := false
		codes := make([]int, 0)
		for _, u := range us {
			for _, d := range u.delo {
				for _, c := range codes {
					if c == d.archiveId {
						exists = true
						break
					}
				}
				if !exists {
					codes = append(codes, d.archiveId)
				} else {
					break
				}
			}
			if exists {
				break
			}
		}
		if exists {
			for _, u := range us {
				fallback.WriteString(u.toString())
				fallback.Sync()
			}
			continue
		}

		dela := make([]*Delo, 0)
		ids := make([]int, 0)
		for _, u := range us {
			ids = append(ids, u.clientId)
			for _, d := range u.delo {
				dela = append(dela, d)
			}
		}
		us[0].delo = dela
		us[0].ids = ids
		clearUsers = append(clearUsers, us[0])
	}

	fmt.Println("All Users: ", count)

	db, err = sql.Open(postgres, destination.uri(postgres))
	check(err)
	db.SetMaxOpenConns(1000)
	db.SetMaxIdleConns(100)
	defer db.Close()

	countries = fillDict("country")
	ranks = fillDict("science_rank")
	educations = fillDict("education")
	topics = fillDict("topic_heading")

	startPersonNum := selectOneInt("select coalesce(max(person_num), 0) + 1 from profile")

	chunkSize := 500
	us := make([]*User, 0)
	result := make(chan int)
	total := len(clearUsers)
	for _, u := range clearUsers {
		if len(us) >= chunkSize {
			go addToNew(us, result)
			us = make([]*User, 0)
		}
		u.number = startPersonNum
		startPersonNum += 1
		us = append(us, u)
	}
	if len(us) > 0 {
		go addToNew(us, result)
	}
	count = 0
	for cnt := range result {
		if count += cnt; count >= total {
			close(result)
		}
	}
	fmt.Println("Inserted all: ", count)
}

/**
 * Получает одно значение из базы - счетчик или id
 */
func selectOneInt(query string) int {
	res := 0
	err := db.QueryRow(query).Scan(&res)
	check(err)
	return res
}

/**
 * Заполняем справочники из базы
 */
func fillDict(table string) []*Dict {
	rows, err := db.Query(fmt.Sprintf("select id, name from %s", table))
	check(err)
	res := make([]*Dict, 0)
	for rows.Next() {
		d := new(Dict)
		err = rows.Scan(&d.id, &d.name)
		check(err)
		res = append(res, d)
	}
	check(rows.Err())
	rows.Close()
	return res
}

/**
 * Вставляем пользователей в новую базу
 * Алгоритм такой - вставляем только, если уж нет похожей записи, если есть то ничего не делаем.
 */
func addToNew(users []*User, res chan int) {
	var ok bool

	for i := range users {
		user := users[i]
		s := make([]string, 0)
		for _, v := range user.ids {
			s = append(s, fmt.Sprintf("%v", v))
		}

		tx, err := db.Begin()
		err = db.QueryRow("select exists(select 1 from profile where '{" + strings.Join(s, ",") + "}' && client_ids)").Scan(&ok)
		check(err)
		if !ok {
			login := translit.EncodeToICAO(
				fmt.Sprintf("%s_%s%s%d%d", strings.ToLower(strings.TrimSpace(user.lastName.String)),
					strings.ToLower(strings.TrimSpace(strings.Split(user.firstName.String, "")[0])),
					strings.ToLower(strings.TrimSpace(strings.Split(user.middleName.String, "")[0])),
					user.birthYear.Int32, rand.Intn(100)))
			check(err)

			cid := getIdByName(countries, strings.TrimSpace(strings.ToLower(user.country.String)))
			rid := getIdByName(countries, strings.TrimSpace(strings.ToLower(user.scienceRank.String)))
			eid := getIdByName(countries, strings.TrimSpace(strings.ToLower(user.education.String)))
			if cid == 0 && user.country.Valid {
				user.remark.String += "\nГражданство: " + user.country.String
			}
			if rid == 0 && user.scienceRank.Valid {
				user.remark.String += "\nУченая степень: " + user.scienceRank.String
			}
			if eid == 0 && user.education.Valid {
				user.remark.String += "\nОбразование: " + user.education.String
			}

			// Создаем account
			_, err := db.Exec("insert into account(login, password, last_name, first_name, middle_name) values($1, $2, $3, $4, $5)",
				login, defaultPassword, user.lastName, user.firstName, user.middleName)
			rollback(err, tx)

			oldNumbers := make([]string, 0)
			for i := range user.delo {
				oldNumbers = append(oldNumbers, user.delo[i].number.String)
			}
			clientIds := make([]string, 0)
			for _, v := range user.ids {
				clientIds = append(clientIds, fmt.Sprintf("%d", v))
			}
			// Заполняем таблицу profile
			accountId := selectOneInt(fmt.Sprintf("select id from account where login = '%s'", login))
			_, err = db.Exec(`insert into profile(account_id, registered, first_reg_date, organization, position, remark, old_numbers, person_num,
							birth_year, country_id, education_id, science_rank_id, client_ids)
						values($1,true,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,'{`+strings.Join(clientIds, ",")+"}')",
				accountId, user.firstRegDate, user.organization, user.position, user.remark, strings.Join(oldNumbers, ","),
				user.number, user.birthYear, sql.NullInt32{Int32: int32(cid), Valid: cid > 0}, sql.NullInt32{Int32: int32(eid), Valid: eid > 0}, sql.NullInt32{Int32: int32(rid), Valid: rid > 0})
			rollback(err, tx)

			// Заполняем таблицу дел, у нас это arch_record
			for _, d := range user.delo {
				archiveId := d.archiveId
				if archiveId == 6 {
					archiveId = 7
				} else if archiveId == 7 {
					archiveId = 6
				}
				mt := sql.NullTime{Valid: false}
				for _, t := range d.thema {
					if t.endDate.Valid && (!mt.Valid || t.endDate.Time.After(mt.Time)) {
						mt = t.endDate
					}
				}
				_, err = db.Exec(`insert into arch_record(profile_id, archive_id, person_num, approve_date)
							values($1, $2, $3, $4)`, accountId, archiveId, fmt.Sprintf("%d-%d", user.number, archiveId), mt)

				rollback(err, tx)

				archRecordId := selectOneInt(fmt.Sprintf("select id from arch_record where profile_id = %d and archive_id = %d", accountId, archiveId))
				// Заполняем таблицу тематик, у нас это research_topic
				for _, t := range d.thema {
					period := ""
					if t.periodStart.Valid && t.periodStart.Int64 > 0 {
						period = fmt.Sprintf("%d", t.periodStart.Int64)
					}
					if t.periodEnd.Valid && t.periodEnd.Int64 > 0 {
						if period != "" {
							period += "-"
						}
						period = fmt.Sprintf("%s%d", period, t.periodEnd.Int64)
					}
					_, err = db.Exec(`insert into research_topic(arch_record_id, theme_name, period, target, start_date, end_date)
							values($1, $2, $3, $4, $5, $6)`, archRecordId, t.name, period, t.target, t.startDate, t.endDate)
					rollback(err, tx)
				}

			}
		}
		check(tx.Commit())
	}
	res <- len(users)
}

func selectUsers() map[string][]*User {
	rows, err := db.Query(`select c.lkod as client_id, c.Fam as last_name, c.Name as first_name, c.Otch as middle_name, c.GR as birth_year,
       c.Country as country, c.FirstRegDate as first_reg_date, c.WorkPlace as organization, c.Job as position, c.Edu as education,
       c.Title as science_rank, c.Comments as remark, l.LdID as delo_id, convert(varchar(12), l.LdNum)+l.LdLitera as delo_number, l.ArhivCode as archive_id,
       t.TKName as theme_name, t.hbdate as period_start, t.hfdate as period_end, t.purp as target, t.wbdate as start_date,
       t.wfdate as end_date from Clients c join Ldelo l on c.lkod = l.LKod left outer join Tk t on l.LdID = t.LdID
		where L.Regdate >= cast('04/01/2013' as datetime) and l.ArhivCode != 5 order by c.lkod, l.LdID`)
	check(err)
	defer check(rows.Err())
	defer rows.Close()
	users := make(map[string][]*User)
	var (
		user *User
		delo *Delo
		key  string
	)
	for rows.Next() {
		u := new(UserRaw)
		err := rows.Scan(&u.clientId, &u.lastName, &u.firstName, &u.middleName, &u.birthYear, &u.country,
			&u.firstRegDate, &u.organization, &u.position, &u.education, &u.scienceRank, &u.remark,
			&u.deloId, &u.deloNumber, &u.archiveId, &u.themeName, &u.periodStart, &u.periodEnd, &u.target,
			&u.startDate, &u.endDate)
		check(err)
		if user == nil || user.clientId != u.clientId {
			user = new(User)
			user.clientId = u.clientId
			user.lastName = u.lastName
			user.firstName = u.firstName
			user.middleName = u.middleName
			user.birthYear = u.birthYear
			user.country = u.country
			user.organization = u.organization
			user.position = u.position
			user.education = u.education
			user.scienceRank = u.scienceRank
			user.remark = u.remark
			user.delo = make([]*Delo, 0)
			key = fmt.Sprintf("%v%v%v%v", user.birthYear.Int32,
				strings.ToLower(strings.TrimSpace(user.lastName.String)),
				strings.ToLower(strings.TrimSpace(user.firstName.String)),
				strings.ToLower(strings.TrimSpace(user.middleName.String)))
			_, ok := users[key]
			if !ok {
				users[key] = make([]*User, 0)
			}
			users[key] = append(users[key], user)
		}
		if delo == nil || delo.id != u.deloId {
			delo = new(Delo)
			delo.id = u.deloId
			delo.archiveId = u.archiveId
			delo.number = u.deloNumber
			delo.thema = make([]*Thema, 0)
			user.delo = append(user.delo, delo)
		}
		if u.themeName.Valid || u.periodStart.Valid || u.periodEnd.Valid || u.target.Valid || u.startDate.Valid || u.endDate.Valid {
			thema := new(Thema)
			thema.name = u.themeName
			thema.periodStart = u.periodStart
			thema.periodEnd = u.periodEnd
			thema.target = u.target
			thema.startDate = u.startDate
			thema.endDate = u.endDate
			delo.thema = append(delo.thema, thema)
		}
	}
	return users
}
