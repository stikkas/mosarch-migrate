package main

import (
	"database/sql"
	"fmt"
	"github.com/akamensky/argparse"
	"strings"
)

type PropertyConnection struct {
	host, port, db, user, password *string
}

func NewPropertyConnection(host [4]string, port [4]string, db [4]string, user [4]string, password [4]string,
	parser *argparse.Parser) *PropertyConnection {
	return &PropertyConnection{
		host:     parser.String(host[0], host[1], &argparse.Options{Default: host[2], Help: host[3]}),
		port:     parser.String(port[0], port[1], &argparse.Options{Default: port[2], Help: port[3]}),
		db:       parser.String(db[0], db[1], &argparse.Options{Default: db[2], Help: db[3]}),
		user:     parser.String(user[0], user[1], &argparse.Options{Default: user[2], Help: user[3]}),
		password: parser.String(password[0], password[1], &argparse.Options{Default: password[2], Help: password[3]}),
	}
}

type Thema struct {
	name        sql.NullString
	periodStart sql.NullInt64
	periodEnd   sql.NullInt64
	target      sql.NullString
	startDate   sql.NullTime
	endDate     sql.NullTime
}
type Dict struct {
	id   int
	name string
}
type Delo struct {
	id        int
	number    sql.NullString
	archiveId int
	thema     []*Thema
}

type UserRaw struct {
	clientId     int
	lastName     sql.NullString
	firstName    sql.NullString
	middleName   sql.NullString
	birthYear    sql.NullInt32
	country      sql.NullString
	firstRegDate sql.NullTime
	organization sql.NullString
	position     sql.NullString
	education    sql.NullString
	scienceRank  sql.NullString
	remark       sql.NullString
	deloId       int
	deloNumber   sql.NullString
	archiveId    int
	themeName    sql.NullString
	periodStart  sql.NullInt64
	periodEnd    sql.NullInt64
	target       sql.NullString
	startDate    sql.NullTime
	endDate      sql.NullTime
}

type User struct {
	clientId     int
	lastName     sql.NullString
	firstName    sql.NullString
	middleName   sql.NullString
	birthYear    sql.NullInt32
	country      sql.NullString
	firstRegDate sql.NullTime
	organization sql.NullString
	position     sql.NullString
	education    sql.NullString
	scienceRank  sql.NullString
	remark       sql.NullString
	delo         []*Delo
	number       int
	ids          []int
}

func (v PropertyConnection) uri(prefix string) string {
	if prefix == sqlserver {
		return fmt.Sprintf("%v://%v:%v@%v:%v?database=%v&encrypt=disable", prefix, *v.user, *v.password, *v.host, *v.port, *v.db)
		//return fmt.Sprintf("%v://%v:%v@%v/%v?database=rh&encrypt=disable", prefix, *v.user, *v.password, *v.host, *v.db)
	} else {
		return fmt.Sprintf("%v://%v:%v@%v:%v/%v?sslmode=disable", prefix, *v.user, *v.password, *v.host, *v.port, *v.db)
	}
}

func (u *User) toString() string {
	archives := make([]string, 0)
	for _, d := range u.delo {
		archives = append(archives, fmt.Sprintf("archive: %d, number: %s", d.archiveId, d.number.String))
	}
	return fmt.Sprintf(
		`=======================================
lkod: %d 
%s %s %s %d
Delo: [%s]
---------------------------------------
`, u.clientId, u.lastName.String, u.firstName.String, u.middleName.String, u.birthYear.Int32, strings.Join(archives, "; "))
}
